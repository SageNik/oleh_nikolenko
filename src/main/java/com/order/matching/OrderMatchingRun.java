package com.order.matching;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class OrderMatchingRun {

    private static List<Order> orders = new ArrayList<>();
    private static List<Trade> trades = new ArrayList<>();

    public static void main(String[] args) {

        System.out.println("Please enter you order (EXAMPLE: BUY 100 50   where 100 - price and 50 - quantity), or LIST" +
                " for view all order list, or CLEAR for cleaning trade history");

        BufferedReader consoleInputReader = null;

        try {
            consoleInputReader = new BufferedReader(new InputStreamReader(System.in));
            String input = "";
            String quit = "q";
            String commandList = "LIST";
            String commandClear = "CLEAR";
            while (!quit.equalsIgnoreCase(input)) {
                input = consoleInputReader.readLine();
                String[] parseInput = input.split(" ");
                if (parseInput.length == 3) {
                    try {
                        Order order = Order.getOrder(parseInput[0], Long.valueOf(parseInput[1]), Integer.valueOf(parseInput[2]));
                        trades.addAll(order.tryTrade(orders));
                        System.out.println("OK");
                    } catch (OrderNotFoundException e) {
                        System.out.println("ERR: Sorry order hasn't been found");
                    }

                } else if (parseInput.length == 1) {
                    if (commandList.equalsIgnoreCase(input)) {
                        orders.forEach(System.out::print);
                        trades.forEach(System.out::print);
                    } else if (commandClear.equalsIgnoreCase(input)) {
                        trades.clear();
                        System.out.println("Trade list has been successfully cleared");
                    } else {
                        System.out.println("ERR");
                    }
                } else {
                    System.out.println("ERR");
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (consoleInputReader != null) {
                try {
                    consoleInputReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
