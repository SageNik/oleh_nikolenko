package com.order.matching;

import java.util.List;

public abstract class Order extends BaseEntity {

    public Order(String name, long price, int quantity) {
        super(name, price, quantity);
    }

    public static Order getOrder(String orderName, long price, int quantity) throws OrderNotFoundException {
        switch (orderName) {
            case "BUY":
                return new Buy(price, quantity);
            case "SELL":
                return new Sell(price, quantity);
            default:
                throw new OrderNotFoundException();
        }
    }

    public abstract List<Trade> tryTrade(List<Order> orders) throws OrderNotFoundException;

    protected int evaluate(List<Order> orders, int currentQuantity, List<Trade> result, List<Order> currentOrders) throws OrderNotFoundException {
        for (Order currentOrder : currentOrders) {
            if (currentOrder.getQuantity() - currentQuantity == 0) {
                orders.remove(currentOrder);
                result.add(new Trade(this.getPrice(), this.getQuantity()));
                currentQuantity = 0;
                break;
            } else if (currentOrder.getQuantity() - currentQuantity < 0) {
                orders.remove(currentOrder);
                result.add(new Trade(this.getPrice(), currentOrder.getQuantity()));
                currentQuantity -= currentOrder.getQuantity();
                break;
            } else if(currentOrder.getQuantity() - currentQuantity > 0){
                orders.remove(currentOrder);
                orders.add(Order.getOrder(currentOrder.getName(), currentOrder.getPrice(), currentOrder.getQuantity() - currentQuantity));
                result.add(new Trade(this.getPrice(), currentQuantity));
                currentQuantity = 0;
                break;
            }
        }
        return currentQuantity;
    }
}