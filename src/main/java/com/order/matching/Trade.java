package com.order.matching;

public class Trade extends BaseEntity{

    public Trade(long price, int quantity) {
        super("TRADE", price, quantity);
    }
}
