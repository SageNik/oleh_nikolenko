package com.order.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Buy extends Order{

    public Buy(long price, int quantity) {
        super("BUY", price, quantity);
    }

    @Override
    public List<Trade> tryTrade(List<Order> orders) throws OrderNotFoundException {
        int currentQuantity = this.getQuantity();
        List<Trade> result = new ArrayList<>();

        do {
            List<Order> sellOrders = orders.stream()
                    .filter(it -> it.getName().equalsIgnoreCase("SELL") && it.getPrice() <= this.getPrice())
                    .collect(Collectors.toList());
            currentQuantity = evaluate(orders, currentQuantity, result, sellOrders);
        }while (currentQuantity != 0 && currentQuantity != this.getQuantity());
        if(result.isEmpty()){
            orders.add(this);
        }
        return result;
    }

}
