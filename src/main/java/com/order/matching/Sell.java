package com.order.matching;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Sell extends Order{

    public Sell(long price, int quantity) {
        super("SELL", price, quantity);
    }

    @Override
    public List<Trade> tryTrade(List<Order> orders) throws OrderNotFoundException {
        int currentQuantity = this.getQuantity();
        List<Trade> result = new ArrayList<>();

        do {
            List<Order> buyOrders = orders.stream()
                    .filter(it -> it.getName().equalsIgnoreCase("BUY") && it.getPrice() >= this.getPrice())
                    .collect(Collectors.toList());
            currentQuantity = evaluate(orders, currentQuantity, result, buyOrders);
        }while (currentQuantity != 0 && currentQuantity != this.getQuantity());
        if(result.isEmpty()){
            orders.add(this);
        }
        return result;
    }
}
